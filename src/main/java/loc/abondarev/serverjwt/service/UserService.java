package loc.abondarev.serverjwt.service;

import loc.abondarev.serverjwt.domain.Role;
import loc.abondarev.serverjwt.domain.User;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final List<User> users;

    public UserService() {
        this.users = List.of(
                new User("anton", "12345", "Антон", "Иванов", Collections.singleton(Role.USER)),
                new User("ivan", "12345", "Сергей", "Петров", Collections.singleton(Role.ADMIN))
        );
    }

    public Optional<User> findByLogin(String login) {
        return this.users.stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst();
    }
}
