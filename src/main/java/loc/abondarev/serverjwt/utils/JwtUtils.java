package loc.abondarev.serverjwt.utils;

import io.jsonwebtoken.Claims;
import loc.abondarev.serverjwt.domain.JwtAuthentication;
import loc.abondarev.serverjwt.domain.Role;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtUtils {
    public static JwtAuthentication generate(Claims claims) {
        JwtAuthentication authentication = new JwtAuthentication();
        authentication.setUsername(claims.getSubject());
        authentication.setFirstName(claims.get("firstName", String.class));
        authentication.setRoles(getRoles(claims));
        return authentication;
    }

    private static Set<Role> getRoles(Claims claims) {
        final List<String> roles = claims.get("roles", List.class);
        return roles.stream()
                .map(Role::valueOf)
                .collect(Collectors.toSet());
    }
}
